# Thesis code
This repository contains the main codebase for my Master's Thesis project on Semi-supervised point cloud classification using LDLS.

# Installation
In order to be able to use everything I did for my thesis, you need to have opencv, laspy, sklearn, numba and cupy installed for Python, either through pip or your package manager. Additionally, Detectron2 needs to be installed, for which the specific method changes regularly. The latest information can be found on [their GitHub page](https://github.com/facebookresearch/detectron2). 

Numba, cupy and Detectron2 are dependent on Cuda in varying degrees, so this needs to be installed as well. For more information see [Nvidias website](https://developer.nvidia.com/cuda-downloads).

# Usage
The code mainly speaks for itself. I have included (or am in the process of including) a configuration section in the code so that it is easier to get the exact results you want or need.

The lidar_segmentation.py file is the main file used to do the classification / segmentation. The other files are scripts that I used to augment the data created or do postprocessing to create nicer images / videos.

In order to use this code, I assume the necessary data is structured as it was delivered to me by Fugro. This means the following folder structure:
```
Vision Data
+---General
+---LAZ
|   +---2019-04-30_b_10
|   \---2019-04-30_b_9
+---ShapeFiles
|   +---LAZ
|   +---Scope
|   \---Video
\---Video
    +---Calibration
    +---ep11-201002303-20190430-075921
    |   +---cam1
    |   |   \---cam1
    |   +---cam2
    |   |   \---cam2
    |   \---cam3
    |       \---cam3
    +---ep11-201002303-20190430-080329
    |   +---cam1
    |   |   \---cam1
    |   +---cam2
    |   |   \---cam2
    |   \---cam3
    |       \---cam3
    +---ep11-201002303-20190430-080712
    |   +---cam1
    |   |   \---cam1
    |   +---cam2
    |   |   \---cam2
    |   \---cam3
    |       \---cam3
    \---externalOrientation
        +---ep11-201002303-20190430-075921
        +---ep11-201002303-20190430-080329
        \---ep11-201002303-20190430-080712
```