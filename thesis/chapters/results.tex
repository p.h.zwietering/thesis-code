\section{Results and evaluation}
\label{sec:results}

In this section we will show qualitative results for the inference of Detectron2 on video frames of various railway scenes. We will also show qualitative results of both the projection method and LDLS. Then we will show the limited quantitative analysis we have been able to do, together with running times of different methods on various sizes of point cloud tiles. For all quantitative results, we will show the interesting results in this section. Additional images, giving a more general overview of the results, will be shown in the appendices.

\subsection{Detectron2 panoptic inference}
\label{sec:results_inference}

First, we will show just the inference output of the chosen panoptic segmentation model on Fugro camera data, together with the corresponding input images. After that, inference results will be shown of the model on RailSem19 images, together with the ground truth labelling of these images and the input images. The labelling of the panoptic segmentation model and the labelling used by RailSem19 does not have a 1-1 mapping, but it does give an impression of the usefulness and accuracy of the panoptic segmentation model.

\subsubsection{Panoptic segmentation inference on Fugro video data}
The output of Detectron2 gives coloured planes on top of the input images, where each colour corresponds to a certain label. The label is shown at least once per present label in the image. Because of the interest in semantic segmentation, all instance segmentation labels are disregarded and shown as a ``things'' label instead. 

The images in Figures \ref{fig:results_station_cam1}, \ref{fig:results_station_cam2} and \ref{fig:results_station_cam3}, show the inference of the panoptic segmentation model on all 3 camera views in the busiest part along the track: the only train station available in the Fugro dataset. A lot of different objects and structures are visible in these images, but the model still manages to give a fairly nice rough segmentation.

There are two main remarks regarding these images. First, for the inference of the third camera viewpoint, the platform is not labelled as platform, but instead it is described as both pavement and wall. While technically this is correct, this is not desirable. For the inference of other images before and after this particular viewpoint, this outcome is similar. We think this is because of the short distance between the camera and the platform and the lack of railway in the image, which the model might need in the picture to understand that the image is taken in a railway environment and thus assign platform labels more easily.

Second, and this is behaviour that holds for all inference images, the quality of the segmentation seems to degrade the further an object or point is placed away from the camera viewpoint. This is most visible in the second camera viewpoint, where we can see far in the distance along the track. We can see that a lot of the area above the railway tracks is erroneously labelled as railway, while it is usually something else, such as wiring or wayside objects.

\begin{figure}[h]
	\centering
	\begin{subfigure}[b]{0.49\textwidth}
		\centering
		\includegraphics[width=\textwidth]{results/station_cam1}
	\end{subfigure}
	\hfill
	\begin{subfigure}[b]{0.49\textwidth}
		\centering
		\includegraphics[width=\textwidth]{results/inference_station_cam1}
	\end{subfigure}
	\caption{View of camera 1 of part of the station with Detectron2 inference.}
	\label{fig:results_station_cam1}
\end{figure}

\begin{figure}[h]
	\centering
	\begin{subfigure}[b]{0.49\textwidth}
		\centering
		\includegraphics[width=\textwidth]{results/station_cam2}
	\end{subfigure}
	\hfill
	\begin{subfigure}[b]{0.49\textwidth}
		\centering
		\includegraphics[width=\textwidth]{results/inference_station_cam2}
	\end{subfigure}
	\caption{View of camera 2 of part of the station with Detectron2 inference.}
	\label{fig:results_station_cam2}
\end{figure}

\begin{figure}[h]
	\centering
	\begin{subfigure}[b]{0.49\textwidth}
		\centering
		\includegraphics[width=\textwidth]{results/station_cam3}
	\end{subfigure}
	\hfill
	\begin{subfigure}[b]{0.49\textwidth}
		\centering
		\includegraphics[width=\textwidth]{results/inference_station_cam3}
	\end{subfigure}
	\caption{View of camera 3 of part of the station with Detectron2 inference.}
	\label{fig:results_station_cam3}
\end{figure}

We have also included a few pairs of images to illustrate the kinds of situations where the segmentation model does seem to have a more significant problem to generate a proper labelling. These are shown in Figures \ref{fig:results_inference_electrical_box} and \ref{fig:results_inference_fence}. Figure \ref{fig:results_inference_electrical_box} shows the view of the second camera on a railway crossing, with on the side of the track a big electrical box. Because of the ambiguity of what this electrical box looks like and the presence of a warning sign on the box, it gets a very inaccurate combination of a lot of small sized labelled areas. While some of these labels would not be very incorrect, the fact that the model designates all of those in small quantities makes this behaviour undesirable.

Figure \ref{fig:results_inference_fence} shows the view of camera 1 on a fence in front of the ramp of a bridge that has just been passed. While the segmentation model correctly recognizes the presence of the fence, it is not segmented completely as we would expect. In addition, the model incorrectly classifies the ramp of the bridge as a mountain.

\begin{figure}[h]
	\centering
	\begin{subfigure}[b]{0.49\textwidth}
		\centering
		\includegraphics[width=\textwidth]{results/electrical_box_cam}
	\end{subfigure}
	\hfill
	\begin{subfigure}[b]{0.49\textwidth}
		\centering
		\includegraphics[width=\textwidth]{results/electrical_box_inference}
	\end{subfigure}
	\caption{Camera image and inference of part of the track with a big electrical box.}
	\label{fig:results_inference_electrical_box}
\end{figure}

\begin{figure}[h]
	\centering
	\begin{subfigure}[b]{0.49\textwidth}
		\centering
		\includegraphics[width=\textwidth]{results/fence_cam}
	\end{subfigure}
	\hfill
	\begin{subfigure}[b]{0.49\textwidth}
		\centering
		\includegraphics[width=\textwidth]{results/fence_inference}
	\end{subfigure}
	\caption{Camera image and inference of part of the track with a fence and somewhat irregular background.}
	\label{fig:results_inference_fence}
\end{figure}

\clearpage

\subsubsection{Panoptic segmentation inference on RailSem19 data}
The results of the panoptic segmentation of the model on RailSem19 image data has the same format as for the Fugro data. For reference to its ground truth, we also included the ground truth semantic segmentation as labelled by the creators of RailSem19. The semantic labels differ between these two data sources, but the outlines of the segments can be roughly qualitatively compared to get an idea what the strengths and weaknesses of the panoptic segmentation model are. For each result, we include the original photo taken, coupled with the ground truth of RailSem19 and the panoptic segmentation inferred by the Detectron2 model.

Figure \ref{fig:results_railsem_track} shows the inference output of a very wide part of railway with multiple tracks. In the RailSem19 dataset, all those tracks are neatly outlined and distinguished from the gravel and sleepers. This is not the case for the panoptic segmentation model, where only the complete area of the railway is detected and labelled as such. Compared with the ground truth, the model does seem to separate the different entities in the background fairly well.

\begin{figure}[h]
	\centering
	\begin{subfigure}[b]{0.32\textwidth}
		\centering
		\includegraphics[width=\textwidth]{results/railsem_track_input}
		\caption{Input image.}
	\end{subfigure}
	\hfill
	\begin{subfigure}[b]{0.32\textwidth}
		\centering
		\includegraphics[width=\textwidth]{results/railsem_track_inference}
		\caption{Inference by model.}
	\end{subfigure}
	\hfill
	\begin{subfigure}[b]{0.32\textwidth}
		\centering
		\includegraphics[width=\textwidth]{results/railsem_track_truth}
		\caption{Ground truth.}
	\end{subfigure}
	\caption{Comparison of RailSem19 labelling and panoptic segmentation model of a clear railway scene with multiple of tracks.}
	\label{fig:results_railsem_track}
\end{figure}

RailSem19 has the advantage of having a varied number of railway scenarios, such as in Figures \ref{fig:results_railsem_track}, \ref{fig:results_railsem_tunnel} and \ref{fig:results_railsem_night}. In Figure \ref{fig:results_railsem_tunnel}, we can see that the panoptic segmentation completely breaks down. The glare of the light in the camera probably makes the problem more severe. But this is a clear failure of the model, which might suggest it is overfitted or not trained on enough different railway scenes. It is especially strange that the model does not even recognize sky or the railway itself.

\begin{figure}[h]
	\centering
	\begin{subfigure}[b]{0.32\textwidth}
		\centering
		\includegraphics[width=\textwidth]{results/railsem_tunnel_input}
		\caption{Input image.}
	\end{subfigure}
	\hfill
	\begin{subfigure}[b]{0.32\textwidth}
		\centering
		\includegraphics[width=\textwidth]{results/railsem_tunnel_inference}
		\caption{Inference by model.}
	\end{subfigure}
	\hfill
	\begin{subfigure}[b]{0.32\textwidth}
		\centering
		\includegraphics[width=\textwidth]{results/railsem_tunnel_truth}
		\caption{Ground truth.}
	\end{subfigure}
	\caption{Comparison of RailSem19 labelling and panoptic segmentation model of a tunnel railway scene.}
	\label{fig:results_railsem_tunnel}
\end{figure}

Another important variation are the weather conditions and time of day. In Figure \ref{fig:results_railsem_night}, a nightly railway scene is tested. The segmentation model recognizes the darkness as sky and still manages to recognize a part of the railway. In contrast with the previous highlighted images, where a tunnel completely broke the segmentation, this was a surprising result.

\begin{figure}[h]
	\centering
	\begin{subfigure}[b]{0.32\textwidth}
		\centering
		\includegraphics[width=\textwidth]{results/railsem_night_input}
		\caption{Input image.}
	\end{subfigure}
	\hfill
	\begin{subfigure}[b]{0.32\textwidth}
		\centering
		\includegraphics[width=\textwidth]{results/railsem_night_inference}
		\caption{Inference by model.}
	\end{subfigure}
	\hfill
	\begin{subfigure}[b]{0.32\textwidth}
		\centering
		\includegraphics[width=\textwidth]{results/railsem_night_truth}
		\caption{Ground truth.}
	\end{subfigure}
	\caption{Comparison of RailSem19 labelling and panoptic segmentation model of a railway scene at night.}
	\label{fig:results_railsem_night}
\end{figure}

Further results can be seen in appendix \ref{sec:appendix_fugro} for the inference on Fugro video data and appendix \ref{sec:appendix_railsem} for the inference on RailSem19 images. The Fugro video images are put in as reference and to show that the model mostly seems to hold up fairly well on this specific type of railway scenes. For the RailSem19 data, we have tried to put in more unique railway scenarios, to try and show whether the panoptic segmentation model breaks down or not in those situations.


\subsection{Point projection}

In order to demonstrate at least to some extent the correctness of the point projection step, we show multiple images of the point projection of the Fugro railway scenes. All points in a circle with a radius of 20 meters and a center at the camera viewpoint are taken and processed such as explained in the methodology in Section \ref{sec:methodology}. These are then projected onto the image as a very small circle with OpenCV. One of these results can be see in Figure \ref{fig:unlabelled_projection}. This image shows a Fugro railway scene from the front facing camera. The lack of points at the white sign is also very noticable, except for the pole that it is attached to, which is probably thicker than the sign itself and therefore registered better. When looking at the catenary wires, we can see that on the right side of the image the points do not seem to lie exactly on the right place in the image. This highlights the importance for LDLS to not just take the pixels the points are projected on, but a square area of pixels around the projected point instead.


\begin{figure}[h]
	\centering
	\includegraphics[width=0.5\textwidth]{results/unlabelled_projection}
	\caption{Projected point cloud points on input image.}
	\label{fig:unlabelled_projection}
\end{figure}


\subsection{LDLS versus labelled point projection}
We will show multiple views of parts of the processed point cloud, and, if applicable, give a corresponding camera view as well. The colouring is based on the Detectron2 default colouring. A label legend is included in Figure \ref{fig:label_legend}. In order to give the exact same views for all different experiments, we use viewpoints in the free point cloud program CloudCompare, which gives us the ability to save the point of view and use it to view different point clouds from the same place. The results with short viewing distance of 11 meters are based on the combined point clouds outputs of all 3 camera views, whereas the results with a long viewing distance of upwards to 70 meters are based on only the point cloud output of the second, front facing camera.

\begin{figure}[h]
	\centering
	\includegraphics[width=0.4\textwidth]{label_legend}
	\caption{Point cloud label legend}
	\label{fig:label_legend}
\end{figure}

First, we give an top down overview of a stitched together set of point clouds, together forming a complete overview of the second run of the railway track in Figure \ref{fig:results_overview}. It is impossible to make out any details from this point of view, but it gives a rough overview of what labels are dominating in each experimental setup. What is very striking is that the LDLS methodology gives a lot of empty labelled points, regardless of viewing distance. We can also see that the experiments with high viewing distance give a broader view of the environment around the railway track, which is expected. The labels appear to match pretty well for the two projection experiments, apart from maybe a bit more gravel labels in the short range experiment.

\begin{figure}
	\centering
	\begin{subfigure}[b]{\textwidth}
		\centering
		\includegraphics[width=\textwidth]{results/overview_projection_full_range}
		\caption{Projection method, viewing distance of up to 70 meters.}
	\end{subfigure}
	
	\begin{subfigure}[b]{\textwidth}
		\centering
		\includegraphics[width=\textwidth]{results/overview_ldls_full_range}
		\caption{LDLS method, viewing distance of up to 70 meters.}
	\end{subfigure}
	
	\begin{subfigure}[b]{\textwidth}
		\centering
		\includegraphics[width=\textwidth]{results/overview_projection_short_range}
		\caption{Projection method, viewing distance of 11 meters.}
	\end{subfigure}
	
	\begin{subfigure}[b]{\textwidth}
		\centering
		\includegraphics[width=\textwidth]{results/overview_ldls_short_range}
		\caption{LDLS method, viewing distance of 11 meters.}
	\end{subfigure}
	\caption{Top down overview shots of the different experiments. These were created by simultaneously showing the point cloud results of many processed images.}
	\label{fig:results_overview}
\end{figure}

In Figure \ref{fig:results_station_view} we included a view of the train station from the viewpoint of the camera such as shown in the first image. We can see that the difference between both projection based results is small compared to the difference between the LDLS based results. The biggest visible difference between the projection results is the number of ground points classified as gravel, the number of platform points classified as wall-brick and the difference in classification of the station buildings. For the LDLS method it appears that the viewing distance made a higher visible difference. From this point of view, the high range experiment has yielded a significantly higher number of labelled points than the low range experiment. Also, less ground points are labelled as gravel in the high range experiment.

\begin{figure}
	\centering
	\begin{subfigure}[b]{0.45\textwidth}
		\centering
		\includegraphics[width=\textwidth]{results/station_cam2}
		\caption{Corresponding front facing camera image.}
	\end{subfigure}
	\vfill
	\begin{subfigure}[b]{0.45\textwidth}
		\centering
		\includegraphics[width=\textwidth]{results/station_view_projection_full_range}
		\caption{Projection method, viewing distance of up to 70 meters.}
	\end{subfigure}
	\hfill
	\begin{subfigure}[b]{0.45\textwidth}
		\centering
		\includegraphics[width=\textwidth]{results/station_view_ldls_full_range}
		\caption{LDLS method, viewing distance of up to 70 meters.}
	\end{subfigure}
	
	\begin{subfigure}[b]{0.45\textwidth}
		\centering
		\includegraphics[width=\textwidth]{results/station_view_projection_short_range}
		\caption{Projection method, viewing distance of 11 meters.}
	\end{subfigure}
	\hfill
	\begin{subfigure}[b]{0.45\textwidth}
		\centering
		\includegraphics[width=\textwidth]{results/station_view_ldls_short_range}
		\caption{LDLS method, viewing distance of 11 meters.}
	\end{subfigure}
	\caption{Labelled point clouds and corresponding camera image of the train station along the track, obtained through various methods.}
	\label{fig:results_station_view}
\end{figure}

In Figure \ref{fig:results_start_view}, we show point clouds of a rural part along the track. The differences between the two projection methods are again minimal, except for the number of points labelled as gravel. We can also see that the vegetation appears to be well segmented. The LDLS point clouds are segmented really poorly however. A lot of points are labelled as empty, and the point cloud made with low viewing distance has a lot of the vegetation labelled as gravel and only a small part of the railway actually labelled as railroad. The long range LDLS point cloud does appear to keep more of the vegetation point cloud points in and labelled as such.

\begin{figure}
	\centering
	\begin{subfigure}[b]{0.45\textwidth}
		\centering
		\includegraphics[width=\textwidth]{results/start_track_cam}
		\caption{Corresponding front facing camera image.}
	\end{subfigure}
	\vfill
	\begin{subfigure}[b]{0.45\textwidth}
		\centering
		\includegraphics[width=\textwidth]{results/start_track_projection_full_range}
		\caption{Projection method, viewing distance of up to 70 meters.}
	\end{subfigure}
	\hfill
	\begin{subfigure}[b]{0.45\textwidth}
		\centering
		\includegraphics[width=\textwidth]{results/start_track_ldls_full_range}
		\caption{LDLS method, viewing distance of up to 70 meters.}
	\end{subfigure}
	
	\begin{subfigure}[b]{0.45\textwidth}
		\centering
		\includegraphics[width=\textwidth]{results/start_track_projection_short_range}
		\caption{Projection method, viewing distance of 11 meters.}
	\end{subfigure}
	\hfill
	\begin{subfigure}[b]{0.45\textwidth}
		\centering
		\includegraphics[width=\textwidth]{results/start_track_ldls_short_range}
		\caption{LDLS method, viewing distance of 11 meters.}
	\end{subfigure}
	\caption{Labelled point clouds and corresponding camera image of a rural part of the track, obtained through various methods.}
	\label{fig:results_start_view}
\end{figure}

In Figure \ref{fig:results_fence_clouds}, we show the same fence again as in Figure \ref{fig:results_inference_fence}, but now with corresponding point clouds. We only ran the shorter range experiments on the side cameras, so there are no long range experiments we can show for this camera image. We can see that the errors of the segmentation model are propagated into the point cloud obtained with the projection method. The fence in the point cloud made with LDLS is labelled as empty.

\begin{figure}
	\centering
	\begin{subfigure}[b]{0.45\textwidth}
		\centering
		\includegraphics[width=\textwidth]{results/fence_cam}
		\caption{Corresponding left facing camera image.}
	\end{subfigure}
	\begin{subfigure}[b]{0.8\textwidth}
		\centering
		\includegraphics[width=\textwidth]{results/fence_projection}
		\caption{Projection method, viewing distance of 11 meters.}
	\end{subfigure}
	\begin{subfigure}[b]{0.8\textwidth}
		\centering
		\includegraphics[width=\textwidth]{results/fence_ldls}
		\caption{LDLS method, viewing distance of 11 meters.}
	\end{subfigure}
	\caption{Point cloud points belonging to the fence picture from Figure \ref{fig:results_inference_fence}.}
	\label{fig:results_fence_clouds}
\end{figure}

In general, LDLS does not function well in large connected areas of point cloud. Either the algorithm gives a lot of points an empty label or it pushes one label to many points. We do not know why it gives so many empty points, since the initial state of the LDLS algorithm should be the same as the projection outcome. The only reason we can think of, is that since there can be many points with a lot of labels assigned to them, with each a percentage that is too low for a certain threshold, none of the labels are returned as output in the end. This could also explain why disconnected areas with singular labels remain unchanged by the LDLS algorithm. For example, in Figure \ref{fig:results_start_view}, the left track bed, which is disconnected from the rest of the point cloud, is still completely classified as railway, whereas the right track is not. This holds for both the short and long range experiments. However, this does not explain why the gravel label has such a high prevalence compared to all other labels.

We also think that the way the strips of point cloud are selected in order to fit them in VRAM for the LDLS algorithm might affect the outcome of the algorithm. This is most notable in big point clouds, when the point clouds need to be reduced in size the most. For example in Figure \ref{fig:appendix_strips} in the appendix, we can see a few horizontal strips perpendicular to the track, which can indicate that the way we choose these strips is an issue.

More results can be seen in appendix \ref{sec:appendix_fugro} and videos of the results on \url{https://git.science.uu.nl/p.h.zwietering/thesis-code}, which are a much more suitable medium than images to show generated point clouds in our experience.

\subsection{Label accuracy for platform points}
The platform label is one of the two most relevant semantic labels that the panoptic segmentation model can distinguish for a railway environment. The other label, railway, is not as easily extracted from the Fugro ground truth maps as platform is, which is why the quantitative analysis was only performed for the platform label, as described in Section \ref{sec:platform_extraction}. When only those points are selected, we get two long strips of points in the shape of a right angle, opposite of each other and mirrored in the track. A top down overview of these points is shown in Figure \ref{fig:ground_truth_platform}.

\begin{figure}[h]
	\centering
	\includegraphics[width=\textwidth]{ground_truth_platform}
	\caption{Top down overview of platform points in white, extracted by checking cylinder inclusion along line segments defined in the ground truth Fugro map. In blue, one tile of train station with the most points of the track is included as reference to the length of the platforms.}
	\label{fig:ground_truth_platform}
\end{figure}

Table \ref{tab:accuracy_ground_truth} contains percentages of the occurrence of each of the mentioned labels per type of experiment for the extracted platform points. We did four experiments, in which we tested projection and LDLS, both for a range of up to two tiles (radius of around 70 meters) and a radius of 11 meters around the camera viewpoint. From the table, we can see that just using the projection with a range of 11 meters has the highest accuracy of these four experiments, with an accuracy of 84 percent. Using projection with the extended range has the worst performance, with an accuracy of 36 percent. We can see that for both LDLS and the projection method with viewing distance, the railroad label occurs very often. Apart from the empty label, the most occurring labels are wall-brick and gravel. Labels with fewer than 0.1 \% occurrence in all experiments are excluded from these results.

\begin{table}[h]
	\centering
	\begin{tabular}{|l|c|c|c|c|c|c|c|c|c|}
	\hline
	Label Occurrence (\%) & \begin{turn}{90}Platform\end{turn} & \begin{turn}{90}Wall-Brick\end{turn} & \begin{turn}{90}Railroad\end{turn} & \begin{turn}{90}Gravel\end{turn} & \begin{turn}{90}Empty (LDLS)\end{turn} & \begin{turn}{90}Wall\end{turn} & \begin{turn}{90}Pavement\end{turn} \\
	\hline \hline
	Projection, radius max. 70 meters 	& 35.89 & 1.70 	& 61.36 & 0.062 & 0.0 	& 0.74 	& 0.11  \\ \hline
	LDLS, radius max. 70 meters			& 49.43 & 0.077 & 34.97 & 4.98 	& 8.37 	& 0.001 & 0.0 \\  \hline
	Projection, radius 11 meters		& 84.36 & 8.58 	& 2.44 	& 0.31 	& 0.0 	& 3.82 	& 0.48 \\  \hline
	LDLS, radius 11 meters				& 65.43 & 0.33 	& 0.39 	& 23.59 & 10.06 & 0.005 & 0.0 \\  \hline
	\end{tabular}
	\caption{Label occurrence of points with ground truth platform.}
	\label{tab:accuracy_ground_truth}
\end{table}

\subsection{Algorithm execution times}
The execution times of the different experiments are shown in Table \ref{tab:execution_times}. These are measured per image, excluding reading and writing times of point cloud tiles. This was done for 122 images in total, spread over all runs and cameras. It is important to note that a smaller viewing distance also means less points are projected and put through LDLS. In order to properly compare the two different viewing ranges, we also included the average per-point processing times. These experiments were run on an AMD Ryzen 5600G CPU and NVIDIA GeForce RTX 3070 GPU with 32 GB of RAM. Unsurprisingly, the projection method with the lowest viewing distance has the lowest execution time per image, whereas the LDLS method for the larger point clouds is the slowest. Both methods have a high relative standard deviation, which means there is a lot of variance in the actual run times. This is probably caused by the variance in point cloud sizes, since the relative variance in the per-point processing times is significantly lower.

\begin{table}
	\centering
	\begin{tabular}{|l|c|c|}
	\hline
	Experiment & Av. ex. time (s) & Av. ex. time per point (s) \\ \hline \hline
	Projection, radius max. 70 meters & $10.52 \pm 5.04$ & $6.77 \cdot 10^{-6} \pm 1.47 \cdot 10^{-6}$ \\ \hline
	LDLS, radius max. 70 meters & $56.41 \pm 39.08$ & $3.35 \cdot 10^{-5} \pm 3.69 \cdot 10^{-6}$ \\ \hline
	Projection, radius 11 meters & $13.07 \pm 7.32$ & $3.86 \cdot 10^{-5} \pm 8.94 \cdot 10^{-6}$ \\ \hline
	LDLS, radius 11 meters & $25.61 \pm 14.7$ & $7.53 \cdot 10^{-5} \pm 1.46 \cdot 10^{-5} $ \\ \hline	
	\end{tabular}
	\caption{Average execution times per image for different experiments, spread over 122 images.}
	\label{tab:execution_times}
\end{table}
