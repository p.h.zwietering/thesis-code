\section{Introduction}
\label{sec:introduction}

In this section we will introduce the problem that this thesis is centered around, together with general information about the context of this project, which forms the relevance of this thesis. We will also introduce some basic concepts necessary to understand this thesis. This section is concluded by the research question of this thesis.

\subsection{Research context}

This research project was started in the interest of Fugro. Fugro is a multinational Dutch company that specializes in collecting and processing all kinds of geographical data. This ranges from geochemical and geophysical investigations to geopositioning and geospatial data aggregation, both on land and in marine situations. Historically, Fugro has mainly been involved in soil investigation for exploring the possibilities of natural resource extraction, but in recent years they have started to transition to applying their expertise on similar problems for renewable energy and infrastructure as well. The main application for this thesis is in the area of geospatial data aggregation of infrastructure as well. 

Geospatial data is all data that contains information ascertaining objects, events or other features located near the surface of the earth \cite{ibm_geospatial}. In this thesis, the geospatial data that will be investigated is data representing the local spatial environment of railways. This data comes in the form of point clouds, together with georeferenced camera images and calibration files.

\subsubsection{Point clouds}
A point cloud is a collection of unordered 3-dimensional points. In a geospatial context, a point cloud represents the location of the external surfaces of objects in a certain area. These points can additionally be labelled with all kinds of information, such as for example colour, an object label or any custom label used for different applications. Point clouds in general can be used for many different purposes, such as creating 3D models of single objects and mapping and visualizing the surroundings of certain areas, both inside and outside. Point clouds can be acquired in four different ways \cite{mainsurvey}: by using image-derivation, Light Detection And Ranging (LiDAR) systems, Red Green Blue - Depth (RGB-D) cameras or Synthetic Aperture Radar (SAR) systems. This thesis only looks at point clouds collected with LiDAR; the results when applying the same methods might vary wildly for point clouds acquired by other means. 

More specifically, Fugro collects their point clouds into so called tiles. Tiles are horizontal squares of 25 by 25 square meters, in which any LiDAR points are collected. These tiles are then saved in a LAZ file, which is a compressed file format for storing LiDAR points. Figure \ref{fig:tiles_track} shows a top down overview of all tiles that have a corresponding file with some number of LiDAR points.

\begin{figure}[h]
	\centering
	\includegraphics[width=\textwidth]{tile_track}
	\caption{Top overview of all tiles of this specific railway track Fugro collected LiDAR points on.}
	\label{fig:tiles_track}
\end{figure}

\subsubsection{LiDAR}
LiDAR systems, or laser scanners as they are also called, are systems that use emitted laser beams to measure the distance from the system to surfaces surrounding it. It measures the time it takes for the beam to leave the laser to the time it takes for the refracted light to return to the system, together with the direction of the laser beam at the time the beam was sent, to calculate at what exact point a surface was measured. Also, since the speed of light is orders of magnitude higher than the speed of trains or planes, it can be used while moving on those vehicles while retaining a very high accuracy. To calculate the distance $d$, one uses the light speed $c$ and time $t$ it takes between emitting the laser beam and arrival of the refracted light as follows:

\begin{equation}
d = \frac{c \cdot t}{2}
\end{equation}

LiDAR systems can be deployed in multiple ways. The literature distinguishes between four types of scanning \cite{mainsurvey}: terrestrial laser scanning (TLS), airborne laser scanning (ALS), mobile laser scanning (MLS) and unmanned laser scanning (ULS). TLS is done with a static system, that might only turn around its axes. ALS is operated from an airplane or even satellites. ULS is done using small drones and differs from ALS in that the distance from the system to the surfaces it measures is way smaller; a drone with ULS might fly through and around a factory, whereas a plane equipped with ALS flies a few kilometers high from the surface of the earth that it is surveying. Finally, MLS refers to systems mounted on moving, earthbound vehicles such as trains or cars. The point clouds of these different systems have very different properties, benefits and limitations. The main difference between these methods is the sparsity of the point clouds; ALS will have point clouds that are significantly more sparse than point clouds acquired through TLS.

\subsection{Problem description}
Fugro thus collects point clouds from railway environments. This is done through MLS, by mounting their own RILA system on the back of trains. This system contains a 360$^{\circ}$ LiDAR scanner together with 3 cameras and an accurate GPS system, along with other equipment that is not relevant for this research, to create a complete view of the surrounding area. A detailed image of this system can be seen in Figure \ref{fig:rila}.

\begin{figure}[h]
	\centering
	\includegraphics[width=0.7\textwidth]{rila}
	\caption{Fugro RILA system \cite{rila_image}.}
	\label{fig:rila}
\end{figure}

The 360$^{\circ}$ LiDAR scanner scans surfaces in a circular motion along the plane perpendicular to the railway direction. This produces so called scan lines, which are points that are loosely arranged in a linear pattern when looked upon from a top-down view, such as in Figure \ref{fig:station_with_zoom}. Scanners that scan in the direction of the movement of the vehicle also exist, but they have the disadvantage that larger objects can obstruct the ``view'' of the scanning line of smaller objects. On the other hand, thin wayside objects, such as railway signs, might not be completely detected by the RILA system. An example can is shown in Figures \ref{fig:missing_objects_image} and \ref{fig:missing_objects_cloud}. Putting together a multitude of these scan lines forms a point cloud that forms a scene that is moderately recognizable for human eyes, but harder to interpret by computers. See Figure \ref{fig:camera_views} for an example of the views of the three cameras and Figure \ref{fig:station_with_zoom} for an example of point cloud depicting a train station.

\begin{figure}[h]
	\centering
	\begin{subfigure}[b]{0.32\textwidth}
		\centering
		\includegraphics[width=\textwidth]{results/station_cam1}
		\caption{Left facing camera.}
	\end{subfigure}
	\hfill
	\begin{subfigure}[b]{0.32\textwidth}
		\centering
		\includegraphics[width=\textwidth]{results/station_cam2}
		\caption{Front facing camera.}
	\end{subfigure}
	\hfill
	\begin{subfigure}[b]{0.32\textwidth}
		\centering
		\includegraphics[width=\textwidth]{results/station_cam3}
		\caption{Right facing camera.}
	\end{subfigure}
	\caption{The three camera viewpoints from the back of the train on the only train station present in the Fugro video data.}
	\label{fig:camera_views}
\end{figure}

\begin{figure}[h]
	\centering
	\includegraphics[width=\textwidth]{point_cloud_station_with_scan_lines}
	\caption{Side view of the station point cloud tile. A top down zoomed view is taken to show the scan lines, roughly 3 centimeters apart here, but this distance is dependent on the speed of the train.}
	\label{fig:station_with_zoom}
\end{figure}

\begin{figure}
	\centering
	\begin{minipage}{0.45\textwidth}
		\centering
		\includegraphics[width=\textwidth]{missing_signpost}
		\caption{Cropped video frame of a part of the track. On the left the two wayside objects with exclamation marks are clearly visible, without objects obstructing the laser scanner.}
		\label{fig:missing_objects_image}
	\end{minipage}\hfill
	\begin{minipage}{0.45\textwidth}
		\centering
		\includegraphics[width=\textwidth]{missing_signpost_cloud}
		\caption{Corresponding point cloud slice. Only one of the signposts is (partly) visible.}
		\label{fig:missing_objects_cloud}
	\end{minipage}
\end{figure}

Fugro has numerous use cases for point clouds collected in this way. It uses the laser stripers on the bottom of the RILA system to get a very detailed and accurate reading of the height and shape of the railway track, which can be used to determine the need for maintenance. The use case that is especially relevant for this research project is the mapping of the local environment of the railway. This mapping is a dataset of labelled polygonal shapes describing the location of all relevant objects close to the railway track. An example can be seen in Figure \ref{fig:ground_truth_map}.

\begin{figure}[h]
	\centering
	\includegraphics[width=0.5\textwidth]{ground_truth_map}
	\caption{Typical top down view of maps delivered by Fugro. Different types of objects get different colouring and different corresponding geometrical shapes. Object labels are also denoted when zoomed in enough.}
	\label{fig:ground_truth_map}
\end{figure}

In order to deliver a map suitable for sale, a necessary intermediary step is to label all points in the point clouds according to the object they are a part of. This creates a segmentation of the point cloud, where points close to each other that have the same label form segments of points that form objects. Such a segmentation can then be used to create the desired maps. Therefore, it is of great importance that point clouds are labelled accurately and homogeneously. This brings us to the problem that is central in this thesis: semantically labelling point clouds placed in a railway setting.

\subsubsection{Current solution}
Currently, Fugro uses the following method for labelling the acquired point clouds. First, a smaller, representative subset of the point cloud is labelled by employee with computer assistance. This is done by calculating features of all points, where features are per-point properties such as position, LiDAR intensity or the number of neighbours in a certain radius around the point. These features are then used to group points that have similar features, while visually checking that points are indeed segmented correctly. Hand-labelling points in such a manner takes up a significant amount of time and labour.

Using this piece of reliably labelled point cloud, a neural network is trained on all points that uses the features as input and the point labels as output. This is done for every different RILA project, i.e. the same neural network can not be used for all point clouds Fugro needs to label, due to the apparent significance of regional differences in railway environments. Inference on this neural network can take up to an hour per 25 by 25 square meters of point cloud. Since the network considers all points separately, there can be a lot of noise. The accuracy usually lies somewhere between 90\% - 95 \% on test data, so after inference more computer-assisted manual labelling needs to be done. Accuracy of the labelling is measured simply by checking how many points are labelled correctly compared with the ground truth, divided by the total number of points.

\subsection{Research questions}

Having such clear drawbacks, Fugro has the wish for a methodology that might (partly) replace or complement their current one in the future. In order to develop such an approach, they asked us to explore available options or come up with a solution better than these options. Fugro has a few constraints for this endeavour.

\begin{itemize}
	\item The final labelling should be as accurate as possible. Fugro needs to achieve a 100\% accuracy in order to deliver correct maps. Of course, such an accuracy will never be feasible, but it does indicate that accuracy is important. For this project however, the accuracy should be at least 80\% or higher.
	\item Preprocessing time notwithstanding, the current method of Fugro takes a pretty long time. The largest point cloud tile that Fugro gave us in this project has just shy of 4.5 million points, which means inference of the neural network per point takes around 0.8 milliseconds when assuming the point cloud is not sampled down, which it usually is. This is definitely not quick for neural network standards. Fugro has no hard demands for the execution times, but it would be a success if they could be reduced from a matter of hours to minutes.
	\item Since creating ground truth, i.e. computer-assisted manual point cloud labelling, is so expensive, our approach should have no need for it. This has the consequence that our approach must also be generally applicable for different geographic locations, as long it is still a railway scene\footnote{We will not be able to test this, since only data for one part of track was delivered, located around Ely, England.}. However, training of models on publicly available training datasets is permitted.
	\item Semantic segmentation is preferred over non-semantic segmentation. The difference lies in the meaning behind the labels that are given. Because of the previous constraint, the number of methods that can be applied to achieve semantic segmentation is limited.
	\item The amount of human interaction with the solution should be as low as possible. Tuning parameters and checking the results can take time, more so when there is a great number of parameters.
\end{itemize}

During the literature research for the research proposal, a lot of methods that could be applied to this research project were tried or immediately rejected if their properties conflicted with the constraints above. These methods will still be discussed in Section \ref{sec:related}, in order to give background information and also be able to substantiate why we chose the method we have. This method is called Label Diffusion LiDAR segmentation\cite{Wang_2019} or LDLS for short, and will be discussed in detail in Section \ref{sec:ldls}. The relevant information for now are its claimed properties: this method achieves semantic segmentation in a matter of seconds instead of hours, does not need human interaction and claims to be accurate. To get to a semantic segmentation of point clouds, it uses a semantic segmentation of video data instead. In consultation with Fugro it was decided to investigate LDLS. In conjunction with LDLS, a segmentation model of the image classification framework of Detectron2\cite{wu2019detectron2} was chosen.This has resulted in the following research question for this project.\\

\textit{How does LDLS perform in a railway environment?}\\

In order to check the performance of LDLS, it is necessary to view the operation of its steps separately. This gives rise to the following subquestions:

\begin{itemize}
	\item How well can Detectron2 be applied to achieve accurate semantic segmentation on railway video data?
	\item How well does label diffusion perform when applied to point cloud data of Fugros railway data?
	\item What steps of the LDLS approach are most suitable for augmentation and how?
\end{itemize}

The rest of this thesis contains a description of relevant related work in Section \ref{sec:related}, including the literature research studied for the proposal of this thesis. We describe the methodology used for the experiments in Section \ref{sec:methodology}. The results to these experiments are presented in Section \ref{sec:results}, which mainly consist of qualitative results due to a lack of access to ground truth. Topics for discussion and recommendations are given in Section \ref{sec:discussion}. The thesis is concluded in Section \ref{sec:conclusion}.