\section{Related work}
\label{sec:related}

A lot of research is being done with regards to segmentation and classification of point cloud data. So much so that every few years surveys on the topic are written to bundle the published research up until that point \cite{mainsurvey, nguyen20133d, s19040810, xia2020geometric}. The segmentation methodology is fairly well divided in two main research areas: point cloud segmentation (PCS) and point cloud semantic segmentation (PCSS). The difference between the two is that PCS consists of putting points together in different segments, while PCSS is the process of labelling all points with the object that the point is a part of. In this context, a segment is a subset of locally connected points of the point cloud. So where PCS just creates segments that have no relation with each other, PCSS also groups together these segments into object classes. Each of those classes should contain pairwise disconnected segments. Since Fugro put high emphasis on approach having to be unsupervised, PCS algorithms will be focused upon this thesis and in the rest of this document when talking about segmentation, this refers to PCS, except when specifically talking about semantic segmentation.

The rest of this section is structured as follows. First, we give a short overview of miscellaneous techniques that we tried on actual Fugro data. Then, we will cover the unsupervised segmentation techniques. Then we give a very succinct description of semantic segmentation. Last, we describe methods that aim to fuse camera and lidar data to gain the benefits of both and try to mitigate the shortcomings that come with methods that handle either separate data source.

\subsection{Preliminary research}
During the preliminary research, numerous techniques and methods were looked at to see if they might fit the constraints given by Fugro. Two of these were tested on actual data, to see if they might prove useful for further research. 

The first method was using a supervoxelization algorithm in order to preprocess the data \cite{Lin2018Supervoxel}. Code for this technique can be found at \\\url{https://github.com/yblin/Supervoxel-for-3D-point-clouds}. Supervoxelization aims to divide the point cloud into many small areas of points that have very similar features, without the need of any domain-specific knowledge to give the points those labels. Those supervoxels can then be used as a whole, instead of looking at all points individually. However, we quickly found out that such a preprocessing technique, and thus supervoxelization in general, will not be sufficient for this research. We found that at clear boundaries, supervoxelization works wonderful, but not on smaller, but still crucial different objects, such as the distinction between the railway track and the ground. See Figure \ref{fig:supervoxelization} for an example of this algorithm on a Fugro point cloud tile.

\begin{figure}[h]
	\centering
	\includegraphics[width=0.8\textwidth]{supervoxelization}
	\caption{Example of the supervoxelization method presented in \cite{Lin2018Supervoxel}.}
	\label{fig:supervoxelization}
\end{figure}

The other approach we tried, was to use surface generation on the point clouds to generate surface meshes, for which there also many different supervised and unsupervised algorithms that can do segmentation. Open3D is an open-source library meant to support many tools useful in 3D data analysis and creation. It has multiple surface generation algorithms \cite{Zhou2018}, the most modern of which is Poisson surface reconstruction \cite{kazhdan2006poisson}. The benefit of Poisson surface reconstruction over other methods in this library, is that it can create non-smooth surface, which are generally present in a railway environment. However, when we applied this algorithm to the Fugro data, we quickly found that it is not useful for this type of data. It does not handle small detached objects well, such as the catenary wires, it shows weird behaviour on sharp edges and is not accurate in general. The way the algorithm generates a surface is by finding an optimal vertex between a number of neighbouring points. This means that surfaces are creating in between points, and not on the actual surface of them. For general purposes, this is not a concern, but customers of Fugro need to have millimeter precision, which this method can not guarantee. For an example of the meshes generated this way, see Figure \ref{fig:surface_reconstruction}.

\begin{figure}[h]
	\centering
	\includegraphics[width=0.8\textwidth]{surface_reconstruction}
	\caption{Example of Poisson surface reconstruction applied to Fugro rail data.}
	\label{fig:surface_reconstruction}
\end{figure}


\subsection{Point cloud segmentation}
PCS can be performed in a myriad of ways. Literature often distinguishes the following methods \cite{nguyen20133d, mainsurvey}, roughly in order of discovery: edge-based, region growing, model-based and clustering-based. 

\subsubsection{Edge-based PCS}
Edge-based PCS is derived from the 2-dimensional edge-based methods that were invented to tackle image segmentation. The shapes of objects can be described by their edges, therefore the idea is that if one can find all edges, one can find all objects. In the case of image segmentation, finding the edges themselves can be done by multiple methods \cite{lakshmi2010study}. These usually rely on a core principle to detect a sudden change in intensity between different pixels. This principle still holds for the 3-dimensional case. After finding all edges, the rest of the points are then grouped together into segments based on their similarities. 

Earliest methods use e.g. the local surface curvature \cite{fan1987segmented}, while newer methods use more sophisticated metrics, such as constructing a binary edge map \cite{sappa2001fast}. All these methods have in common that they can be executed fairly quickly. However, the main drawbacks are that edge-based segmentation algorithms can only reliably segment linear and planar point clouds, because other types of shapes are way harder to describe by either planes or lines. For the same reason, this type of segmentation algorithm also can not deal well with outliers and noise.


\subsubsection{Region growing PCS}
Region growing is a segmentation strategy which does precisely as the name suggests. Region growing aims to divide the point cloud into multiple regions by grouping together points that have similar properties. Usually, seed points are chosen in some way, either manually or by a heuristic, which then form the basis of the regions that will be reported as segments. All neighbouring points of the initial seed points are then taken into consideration, to see if they are similar enough to be joined into the region. This decision is based on certain criteria like the intensity and geometric properties of the point neighbourhood. This process is repeated iteratively until all points belong to a region. Some methods also allow the joining of regions \cite{hahnel2003learning, poppinga2008fast, vo2015octree}.

Region growing can perform really well, but requires a lot of parameters to be set up front. Tweaking these parameters, i.e. the growing criteria and initial region seeds, can be a tedious process in the form of trial and error. The result can quickly be either over- or undersegmented. This means there are respectively either too many or too little segments. Oversegmentation always has preference, and is sometimes even done as a preprocessing step, in order to treat the many little segments as some sort of supervoxel in the rest of the segmentation process. Undersegmentation does not have practical uses. Also, because the algorithm decides if points belong to a region based on the current aggregated features of a region, it does not deal well with outliers and noise. 

\subsubsection{Model-based PCS}
Model-based segmentation is divided into two approaches: RANSAC (RANdom SAmple Consensus) and HT (Hough Transform). Both of these approaches try to fit certain geometric models on the point clouds and see how well these perform according to pre-determined criteria. The approaches differ in how they fit models and how their performances are evaluated.\\

\textbf{RANSAC}\\
In general, RANSAC aims to do two things. First it takes random samples from the point cloud (by picking a set of $N$ random points) and then tries to fit geometric mathematical models on these points \cite{schnabel2007efficient, li2017improved, li2021point}. These can be shapes like planes, spheres and toruses: as long as they can be mathematically described by the user. What models specifically are used is a parameter that must be chosen by the user. In the next step, the algorithm tries to pick the best parameters for these models. For example, a spherical model will be represented by $(x - a)^2 + (y-b)^2 + (z-c)^2 = r^2$. So $(a,b,c,r)^T$ are the parameters that have to be evaluated. RANSAC then chooses the most probable parameter values for our points, which is defined as the minimal loss for all parameter values. \\

\textbf{HT}\\
HT consists of three steps. First, all points of the point cloud are transformed into points in a discretized parameter space. Then as the second step, we accumulate the number of points that are contained in a parameter subset by keeping up a score for each eligible set of parameters. In the end the shape parameters are picked that get the highest score. The most simple HT algorithm uses an angle-radius parameterization for plane fitting: $\rho = x\cos(\theta) \sin(\phi) + y\sin(\theta)\sin(\phi) + z\cos(\phi)$, with $\rho$ the distance from the point to the origin and $\theta$ and $\phi$ the polar coordinates of the normal of the plane that goes through the point with coordinates $(x, y, z)$. The major drawback of HT is that even though a discretized parameter space is used, the space itself is unbounded, which can cause very large memory usage and long computation times \cite{rabbani2005efficient,borrmann20113d}.\\

What both of these methods have in common is that they can detect shapes in places that do not actually exist. For example, imagine that there are a lot of points that all lie on a single line segment, but the line segment itself is interrupted by gaps. These methods might mistakenly detect a straight uninterrupted line segment, because there are multiple point that do support the claim that there is a line segment present. However, the flip side is that these methods can handle outliers and noise relatively well. RANSAC is also non-deterministic, because of the random sampling it does.

\subsubsection{Clustering-based PCS}
This category of segmentation algorithms is not as strictly defined as the rest. Loosely speaking, these algorithms all group together points based on some features, be they geometric or something else. One might argue that model-based, edge-based and region growing also do this, but for clustering-based algorithms the features that are segmented upon might not even be known beforehand, which make them quite distinguishable. The main directions of clustering-based PCS are K-means clustering, fuzzy clustering, mean-shift and graph-based approaches.\\

\textbf{K-means clustering}\\
K-means clustering is a very classic algorithm, which has proven itself to be useful for a plethora of use cases \cite{hartigan1979algorithm}. Roughly speaking, K-means clustering is an iterative algorithm in which K unlabelled groups of points are clustered in the chosen feature space. First, these K centre points are chosen randomly, or spread out uniformly. Then, all feature points get assigned to one of the K centres based on distance (which can be Euclidian or something else based on the use case). Then the K centres of all these groups is recalculated, after which these 2 steps are repeated until the solution converges. In the context of PCS, K-means clustering is performed by clustering based on local features of points and distance to the cluster centre combined \cite{saglam2020clustering}. This makes it not very applicable to the segmentation of general scenes, since there different shapes of varying sizes. K-means clustering is very flexible in the sense that it can easily be adapted to handle different features, but it also depends on the regularity of the shapes it considers. It also suffers from the main drawback of K-means clustering: getting the right value for K, which is a recurring problem when using K-means clustering.\\

\textbf{Fuzzy clustering}\\
Fuzzy clustering is very similar to K-means clustering. It differs mainly on once aspect; in fuzzy clustering the sample points can belong to multiple centre points in varying degrees, whereas with K-means clustering the sample points either belong completely to a centre or they do not. The rest of the algorithm is in essence the same as for K-means clustering. As for PCS, this method has primarily been tested on planar buildings, which it did quite well \cite{biosca2008unsupervised}. Not a lot of further research has gone into this.\\

\textbf{Mean shift}\\
Mean shift is a non-parametric clustering algorithm. Therefore, it is very easy to set up: in contrast with K-means clustering one has no need of knowledge of K. However, because in addition to the shape the number of segments is unknown, mean shift tends to oversegment point clouds. Just as with region growing, this does not need to be a problem however \cite{melzer2007non}.\\

\textbf{Graph-based}\\
Graph-based clustering is done in a two-part strategy: graph construction and partitioning. First a graph is constructed on the point cloud, usually based on point features and the local neighbourhood of the $k$ nearest neighbours. This builds a graph on all the points in which the points are the nodes and the edges represent whether or not two points might belong to the same object, just based on proximity. Then, some min-cut algorithm is applied in order to sever the edges that represent a connection between different objects in the point cloud \cite{ural2012min}. These kinds of min-cut algorithms are well researched topics and often can be executed in polynomial time, which makes them quite efficient \cite{strom2010graph}. The intuition behind this approach also makes it an appealing strategy.

\subsection{Point cloud semantic segmentation}
Since Fugro explicitly wants to have some form unsupervised segmentation and labelling of their point clouds, or in the very least, only use publicly available training data\footnote{At the time of writing, no labelled dataset for rails point cloud exists.}, this section will be brief. It is still nice to have some background knowledge of the semantic side of segmentation. The two main research directions are the regular machine learning approaches and the deep learning approaches. The difference between the two is that with deep learning, the techniques used can create their own features that are not immediately present in the training data. Apart from that, the process behind these approaches are very similar. There is some model that is fed training data in the form of global and / or local point features, which the model uses to adjust its internal parameters. This is repeated until the model does not change enough any more that further training is warranted. Then, the model is evaluated on test data instead to see how it performs.

The main challenge on the topic of point cloud segmentation with deep learning is the form of the data\footnote{This seems to not be an issue for regular machine learning, since there the models use each point in the point cloud separately, whereas in deep learning the complete point cloud is used as a sample instead and the idea is to let the network detect patterns.}. Since point clouds are in essence unstructured data, it is not trivial to insert it directly into learning models, which usually need some regularized input type. This can be dealt with in one of the following ways. The first idea is to use a multiview approach, in which 2-dimensional images are taken from the scene in such a way that all points are visible in at least one of these images \cite{su2015multi}. This has major drawbacks, since it is not easy to do this automatically, especially for larger scenes. Also, complex 3-dimensional structures might simply get lost when projected into 2 dimensions. Second, one can regularize the point cloud itself, namely by voxelizing it \cite{maturana2015voxnet}. This is a fairly straightforward process, but it generates a lot of data in the form of empty space that is now registered\footnote{Which can be partly remedied by using a data structure such as octrees.}, while losing information in the form of a reduction loss and it introduces the need of label interpolation. Last, there are deep learning networks that manage to handle point clouds directly. PointNet was the first network that succeeded, which inspired a lot the networks that are still being published \cite{qi2017pointnet}.

\subsection{Camera LiDAR fusion}
Camera LiDAR fusion refers to methods that try to combine both visual imagery and 3-dimensional data in order to to gain advantages of using either data source but reduce the disadvantages that come with them as well. These methods can be both supervised and unsupervised and have lots of different purposes, such as depth completion, object detection and semantic segmentation \cite{cui2021deep}. For this thesis we will focus on one specific research, namely into Label Diffusion LiDAR Segmentation method (LDLS) \cite{Wang_2019}. In the following sections we will lay out the relevant related work that is connected to LDLS.

\subsubsection{LDLS}
\label{sec:ldls}
LDLS aims to perform semantic segmentation of point clouds by using no semantic or a priori knowledge of the point cloud data. Instead, LDLS only needs semantic knowledge of camera data corresponding to the point cloud scenes. Usually there is a substantial lack of annotated point cloud data, which is not the case for annotated image data. These days image datasets like ImageNet \cite{deng2009imagenet}, MS COCO \cite{lin2015microsoft} and Googles Open Images \cite{OpenImages} are freely available to the public and offer millions of labelled images that can be used for all kinds of deep learning purposes. For this thesis, a dataset of labelled railway scene images is probably most relevant, called RailSem19 \cite{railsem}. This dataset consists of 8500 images of both geometrically defined bounding boxes of important railway objects such as switches and rail traffic signs and pixel-based labels on everything that is visible in the images. For this thesis, the geometrically defined ``important'' labels are not useful, because their labels are too specific for Fugro purposes and their bounding boxes are not tight. Their labels are also included more generally in the pixel-based masks anyway. See the figures in appendix \ref{sec:appendix_railsem} for more clarity.

The LDLS approach works as follows \cite{Wang_2019}. As mentioned before, it needs annotated data of relevant images. The method then assumes that a Mask-R-CNN model is trained to learn to draw labelled mask on the test section of that data. Mask-R-CNN is a convolutional neural network approach that can be used to train models to be able to draw object instance segmentation masks on images \cite{he2017mask}. Although this method was developed and published in 2017, it does not hold up to contemporary methods, such as models in the frameworks of Detectron2 \cite{wu2019detectron2} and MMDetection \cite{mmdetection}, both of which were first published in 2019. Using an image semantic segmentation model, the approach then infers the models on the desired images. At the same time, an undirected graph is constructed with nodes corresponding to the point cloud points and nodes corresponding to the image pixels. The total graph $G$ consists of 2 types of nodes, namely the 2D pixel nodes and the 3D point cloud point nodes, both of which can be labelled with the semantic labels from the masking model. There are also 2 types of edges: edges between the 3D nodes and edges between a 2D node and a 3D node. Initially, all 3D nodes are unlabelled, and the 2D nodes get the labels that correspond with the inference labels of the masking model on the images. These subgraphs are denoted $G^{2D\rightarrow 3D}$ and $G^{3D\rightarrow 3D}$ for the 2D to 3D subgraph and 3D to 3D subgraph respectively.

In order to create the 2D to 3D edges, all 3D points are projected to the 2D image, assuming we know the relative position of the image to the point cloud. For details about this projection, see Section \ref{sec:projection}. In order to combat calibration errors and errors due to the lack of depth in the 2D segmentation masks, every 3D node is edge-connected with all the pixel nodes that lie in a 5 pixel by 5 pixel box around its 2D projection. For the 3D to 3D edges, simply a nearest-neighbour graph is constructed. All the weights of both types of existing edges are initialized in a certain way, and the weighted adjacency matrices of both parts of the graph are put together in one, with $G$ represented by Equation \ref{eq:graph_matrix}. This makes the label diffusion easier later on.

\begin{equation}
\label{eq:graph_matrix}
G = \begin{bmatrix}
G^{3D \rightarrow 3D} & G^{2D \rightarrow 3D}\\
0 & I
\end{bmatrix}
\end{equation}

Now the initial graph is set up, the labels can be diffused from the 2D nodes to the 3D nodes. The diffusion itself is actually quite elegant and simple. We define $z^{(m)} \in \mathbb{R}$ as the label vector for label $m$ for all points and pixels, so it has the same length as the width of the complete matrix for $G$. As $z^{(i)}$ is defined to hold the information for one label $i$ for all points, it is initialized for all 3D points as 0 and a 1 if a 2D pixel holds that class label. Then, the only operation needed to diffuse the class labels of the 2D pixels to the 3D points is given in Equation \ref{eq:diffusion}. This is done consecutively until all vectors $z$ converge. It can be proven that they do converge \cite{Wang_2019}. The way that $G$ is constructed ensures that only the 3D node labels are changed after each multiplication.

\begin{equation}
\label{eq:diffusion}
z^{(m)} \leftarrow G \times z^{(m)}
\end{equation}

Since the algorithm so far is still prone to some errors due to the projection of background 3D point cloud points to the front in a segmentation mask, an outlier removal substep is executed in the end. For all class labels the largest connected component is taken, and all points that have the same class label but are not part of the connected component have their class label set to the default 0 again. 

\subsubsection{Semantic image segmentation with Detectron2}
\label{sec:detectron2}
In the area of image segmentation, Facebooks Detectron2 claims to be a state-of-the-art next generation library \cite{wu2019detectron2}. It provides various types of image segmentation and detection networks. For this thesis in particular, the image segmentation models are useful. There is a distinction between 3 types of semantic image segmentation: instance segmentation, semantic segmentation\footnote{The nomenclature of this type in particular is very confusing.} and very recently also panoptic segmentation \cite{kirillov2019panoptic}. 

Instance segmentation is the segmentation task in which countable, distinguishable \textit{things}, such as people, cars or road signs are segmented. In particular, this segmentation task seeks to detect each of these objects and label them separately. Labels will typically consist of a set $(l_i, z_i)$ denoting both the semantic label $l_i \in \mathcal{L}^{th}$ and the instance $z_i \in \mathbb{N}$, where $\mathcal{L}^{th}$ contains all possible thing labels, including a void label for pixels that do not belong to a thing. For pixels belonging to the same object, both $l_i$ and $z_i$ need to be identical. Note that instance segmentation only looks at a select number of semantic labels, thus generally labelling a tiny subset of pixels in an image, since most images will not completely consist of things.

Semantic segmentation on the other hand is a segmentation task in which all \textit{stuff} is distinguished; stuff being all different, amorphous entities visible in an image, e.g. sky, road, railway and vegetation. As such, semantic segmentation seeks to assign a label to each pixel in an image, without distinguishing between different objects of the same label. Semantic segmentation produces only one value per pixel, a label $l_i \in \mathcal{L}^{st}$. $\mathcal{L}^{st}$ denotes all defined stuff labels.

In 2019 panoptic segmentation was introduced to describe the segmentation task that attempts to combine both instance segmentation and semantic segmentation \cite{kirillov2019panoptic}. This segmentation task both labels every pixel of an image and distinguishes between different, countable objects. Pixels get assigned a pair of values $(l_i, z_i)$ similar to instance segmentation. However, if $l_i \ in \mathcal{L}^{st}$, the value of $z_i$ is simply disregarded. The sets of labels $\mathcal{L}^{st}$ and $\mathcal{L}^{th}$ must thus be mutually exclusive and collectively exhaustive.

Instance segmentation is not sufficient for this thesis, as the goal is to classify the complete point cloud, without the need of distinguishing between different object instances. 

The Detectron2 framework used in this thesis does not provide a baseline semantic segmentation network, so in order to still do semantic segmentation on the video images, a panoptic segmentation model is used instead. The chosen model is the COCO Panoptic Segmentation Baseline R101-FPN model, since it achieves the highest accuracies and the difference in inference time with other models is negligible. This model has a ResNet backbone \cite{He2015}, which means that the first 101 layers have a ResNet architecture. ResNet is a very successful neural network architecture that manages to create deeper neural networks without degradation of the backpropagation gradient.

\subsubsection{3D to 2D point projection}
\label{sec:projection}

LDLs uses a projection of the 3D point cloud points onto the 2D images to create an adjacency matrix that is used for further processing. Creating this projection is done using intrinsic and extrinsic calibration parameters of the cameras. Intrinsic parameters are internal camera parameters such as camera focal length and distortion, external parameters include the translation and rotation of the camera compared to the calibration value \cite{szeliski2010computer}. 

Transforming 3-dimensional spatial coordinates to 2-dimensional pixel coordinates is done as follows \cite{szeliski2010computer}. The image position $(x, y)$ of the 3-dimensional point $(X, Y, Z)$ is given by the transformation in Equation \ref{eq:projection_transformation}. $f$ denotes the focal length of the point, which is the distance from the point to the camera plane. This equation only holds in an ideal case, i.e. a camera without any internal or external distortions.

\begin{equation}
\label{eq:projection_transformation}
\begin{pmatrix}
x \\
y \\
f
\end{pmatrix} = \frac{f}{Z} \begin{pmatrix}
X \\
Y \\
Z
\end{pmatrix}
\end{equation}

In order to truly transform from the 3D point to image, we define three coordinate frames: the world coordinate frame $\vec{X}_w$, the camera coordinate frame $\vec{X}_c$ and the image coordinate frame $\vec{p}$. The first two use meters as unit, the image coordinate frame uses pixels. The transformation from $\vec{X}_w$ to $\vec{X}_c$ is calculated as in Equation \ref{eq:world_to_camera_transformation}. $M_{ex}$ is the 3 by 4 extrinsic calibration matrix $M_{ex} = ( R \hspace{5mm} {-R\vec{d}_w})$, where $R$ is the 3 by 3 rotation matrix. $\vec{d}_w$ is the location of the center of the camera in world coordinates.

\begin{equation}
\label{eq:world_to_camera_transformation}
\vec{X}_c = M_{ex}[\vec{X}_w^T, 1]^T
\end{equation}

Then, $X_c$, which is still in world coordinates, is transformed to camera coordinates as in Equation \ref{eq:perspective_transformation}.

\begin{equation}
\label{eq:perspective_transformation}
\vec{x}_c = \frac{f}{X_{3,c}}\vec{X}_c
\end{equation}

Finally, the perspective corrected camera coordinates $\vec{x}_c$ are transformed to pixel coordinates $\vec{p}$ by Equation \ref{eq:camera_to_pixel_transformation}. 

\begin{equation}
\label{eq:camera_to_pixel_transformation}
\vec{p} = \frac{1}{f}M_{in}\vec{x}_c
\end{equation}

Here $M_{in}$ is the intrinsic calibration matrix defined as in Equation \ref{eq:intrinsic_calibration}. These parameters are camera specific. $f_{s_x}$ and $f_{x_y}$ denote the pixel focal length. $o_x$ and $o_y$ denote the offset from the top-left corner of the pixel coordinates compared to the camera coordinate center.

\begin{equation}
\label{eq:intrinsic_calibration}
M_{in} = \begin{pmatrix}
f_{s_x} & 0 & o_x \\
0 & f_{s_y} & o_y \\
0 & 0 & 1
\end{pmatrix}
\end{equation}

Using all three transformations in series, starting with the 3D points, yields the pixel coordinates. Slight variations of these formulas exist, to accommodate a linear transformation, whereas the equations given above give a nonlinear transformation.

