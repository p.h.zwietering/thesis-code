from pathlib import Path
import os

INPUT_FOLDER = Path("C:/Users/Philippe/Documents/studie/master_thesis/output/")

COLOR_DICT = {0: (135, 165, 79), 3: (123,83,83), 11: (112,70,152), 13: (225,200,224), 17: (247,181,191), 19: (216,146,136), 21: (130,76,126), 22: (124,116,95), 30: (123,53,66), 37: (96,126,36), 38: (17,141,40), 39: (139,130,131), 40: (107,155,195), 44: (106,102,99), 46:(137,217,132), 47: (185,197,193), 50: (108,103,12), 52:(103,99,140)}

def main():
    run_folders = [f for f in INPUT_FOLDER.glob("*") if f.is_dir() and f.name[-1].isdigit()]
    
    for run_folder in run_folders:
        input_folders = [f for f in run_folder.glob("*") if f.is_dir()]

        # print(input_folders)

        for folder in input_folders:
            print(folder)

            input_files = [f for f in folder.glob("*.txt")]

            os.makedirs(folder / "added_colors", exist_ok=True)
            for file in input_files:
                print(file)

                input_handler = open(file, 'r')
                output_handler = open(folder / "added_colors" / file.name, 'w').close()
                output_handler = open(folder / "added_colors" / file.name, 'a')

                for line in input_handler:
                    words = line.split()

                    if len(words) != 4:
                        continue

                    try:
                        label = int(words[-1])
                    except:
                        label = 55
                    
                    try:
                        color = COLOR_DICT[label]
                    except:
                        color = (255, 255, 255)
                    
                    for word in words:
                        output_handler.write(word + ' ')
                    
                    for c in color:
                        output_handler.write(str(c) + ' ')
                    output_handler.write('\n')

                input_handler.close()
                output_handler.close()


if __name__ == "__main__":
    main()