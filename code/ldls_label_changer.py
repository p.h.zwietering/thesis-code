from pathlib import Path
import os

INPUT_FOLDER = Path("C:/Users/Philippe/Documents/studie/master_thesis/output/")


def main():
    input_folders = [f for f in INPUT_FOLDER.glob('**') if "ldls" in f.stem]

    for folder in input_folders:
        os.makedirs(folder / "fixed_label", exist_ok=True)

    for folder in input_folders:
        input_files = [f for f in folder.glob("*.txt")]

        print(folder)

        for file in input_files:
            print(file)
            input_handler = open(file, 'r')

            output_path = folder / "fixed_label" / file.name
            output_handler = open(output_path, 'w')
            output_handler.close()
            output_handler = open(output_path, 'a')

            for line in input_handler:
                words = line.split()
                try:
                    label = int(words[-1])
                except:
                    label = 55

                if label == 0:
                    new_label = 55
                elif label == 55:
                    new_label = 55
                else:
                    new_label = label - 1
                
                for word in words[:-1]:
                    output_handler.write(word + ' ')
                output_handler.write(str(new_label) + '\n')
            
            input_handler.close()
            output_handler.close()
        
                

if __name__ == "__main__":
    main()