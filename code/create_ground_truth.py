from turtle import distance
import ezdxf
# from ezdxf.groupby import groupby
from pathlib import Path
import laspy
import numpy as np
import math


GROUND_TRUTH_FILE_PATH = Path("C:/Users/Philippe/Documents/studie/master_thesis/ground_truth/ground_truth_mapping.dxf")

POINT_CLOUD_FILE_FOLDER = Path("C:/Users/Philippe/Documents/studie/master_thesis/Vision Data/LAZ")

OUTPUT_FOLDER = Path("C:/Users/Philippe/Documents/studie/master_thesis/output/")

SPHERE_RADIUS = 0.8

def distance(point_a, point_b):
    result = math.sqrt((point_a[0] - point_b[0])**2 + (point_a[1] - point_b[1])**2 + (point_a[2] - point_b[2])**2)

    return result

def points_in_cylinder(pt1, pt2, r, q):
    vec = pt2 - pt1
    const = r * np.linalg.norm(vec)
    # print(pt1, pt2, q, vec, const)
    # print(np.dot(q - pt1, vec), np.dot(q - pt2, vec),  np.linalg.norm(np.cross(q - pt1, vec)))
    return np.dot(q - pt1, vec) >= 0 and np.dot(q - pt2, vec) <= 0 and np.linalg.norm(np.cross(q - pt1, vec)) <= const

def check_file_necessity(file_name, cylinder_list):
    coordinate_name = file_name.stem
    x = int(coordinate_name[7:17])
    y = int(coordinate_name[-10:])
    
    for point_group in cylinder_list:
        for point_x, point_y, _ in point_group:
            if point_x >= x and point_x <= x + 25 and point_y >= y and point_y <= y + 25:
                return True 
    return False

def main():
    print("Creating ground truth from Fugro dxf mapping")

    doc = ezdxf.readfile(GROUND_TRUTH_FILE_PATH)
    msp = doc.modelspace()

    # group = ezdxf.groupby.groupby(msp, "layer")
    # for layer, entities in group.items():
    #     print(layer)

    # building_lines = msp.query('* [layer=="SU-BD-Buildings-Z"]')
    platform_z_lines = msp.query('* [layer=="SU-BD-Platform-Z"]')
    
    point_list = []

    # print("buildings")
    # for building in building_lines:
    #     point_list.append([])
    #     try:
    #         points = building.points()
    #         # print(type(points))
    #         for point in points:
    #             point_list[-1].append(point)
    #             # print(point)
    #     except:
    #         points = building.vertices_in_wcs()
    #         # print(type(points))
    #         for point in points:
    #             point_list[-1].append(point)
    #             # print(point)
    #     # print("\n")
    
    # print("platforms")
    for platform in platform_z_lines:
        point_list.append([])
        try:
            points = platform.points()
            # print(type(points))
            for point in points:
                point_list[-1].append(point)
                # print(point)
        except:
            points = platform.vertices_in_wcs()
            # print(type(points))
            for point in points:
                point_list[-1].append(point)
                # print(point)
        # print("\n")

    # print(point_list)

    # las = laspy.read(POINT_CLOUD_FILE_PATH)
    laz_files = list(POINT_CLOUD_FILE_FOLDER.rglob("*.laz"))
    output_file_name = OUTPUT_FOLDER / "ground_truth_platforms_cylindric.txt"
    output_file = open(output_file_name, 'w')
    output_file.close()

    output_file = open(output_file_name, 'a')

    for laz_file in laz_files:
        if not check_file_necessity(laz_file, point_list):
            continue
        las = laspy.read(laz_file)

        n = 0
        m = 0
        lidar_points = np.vstack((las.x, las.y, las.z)).transpose()

        for lidar_point in lidar_points:
            point_found = False
            for point_group in point_list:
                for i in range(len(point_group) - 1):
                    # print(points_in_cylinder(point_group[i], point_group[i+1], SPHERE_RADIUS, lidar_point))
                    if points_in_cylinder(point_group[i], point_group[i+1], SPHERE_RADIUS, lidar_point):
                        output_file.write(str(lidar_point[0]) + " " + str(lidar_point[1]) + " " + str(lidar_point[2]) + '\n')

                        n += 1
                        print(n, m, end='\r')
                        point_found = True
                        break
                
                if point_found:
                    break
            
            m += 1
    


if __name__ == "__main__":
    main()