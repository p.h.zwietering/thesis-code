# These all need to be installed by pip (or the method suitable for your particular operating system)
import math
from pathlib import Path
import numpy as np
import cv2
import csv
import xml.etree.ElementTree as et
import laspy
import os
import time

# Detectron needs to be manually installed, can't be done through pip
from detectron2 import model_zoo
from detectron2.config import get_cfg
from detectron2.engine import DefaultPredictor
from detectron2.utils.visualizer import Visualizer, ColorMode
from detectron2.data import MetadataCatalog

# Local file
import ldls

configuration_dict = {
# Input folder, see readme for expected folder structure
"vision_data_path": Path("C:/Users/Philippe/Documents/studie/master_thesis/Vision Data"), 

# Detectron2 path, necessary for labelling
"detectron2_path": Path("C:/Users/Philippe/AppData/Local/Programs/Python/Python39/Lib/site-packages/detectron2"),

# Output folder
"point_cloud_output_folder": Path("C:/Users/Philippe/Documents/studie/master_thesis/output"),

# Which camera views need to be used
"cams_to_use": [2],

# Which folder runs to use, in order they are found in the video data folder
"run_folders_to_use": [0,1,2],

# At which video frame in the folder you want the program to start, useful when only a selection must be executed. Number is checked on "safety"
"video_start": 0,

# At which video frame in the folder you want the program to end, useful when only a selection must be projected. Number is checked on "safety"
"video_end": 100000,

# Step size between video files that need to be projected, in my experience 4 is good enough for 11m, for unlimited it differs but to be safe set it on 10 
"video_step_size": 40,

# Z correction, didn't seem to be necessary in my case
"z_correction": 0.0,

# Detectron2 device to be used. When only doing inference, cpu is good enough
"detectron_device": 'cpu',

# Maximum number of ldls points that fit on the gpu. If the gpu crashes, lower this number.
"max_ldls_points": 300000,

# If the viewing distance needs to be clipped around the camera viewpoint. If not, this is set to at most 2 point cloud tiles (max 50m).
"clip_viewing_distance": False,

# If viewing distance is set to be clipped, the radii can be set here. I found 11 meters to be a reasonable trade off between viewing distance and accuracy
"clipping_radius": [11],

# If speed measurements need to be done or not
"speed_measurements": True,
}

COLOR_DICT = {0: (80,20,120), 3: (123,83,83), 11: (112,70,152), 13: (225,200,224), 17: (247,181,191), 19: (216,146,136), 21: (130,76,126), 22: (124,116,95), 30: (123,53,66), 37: (96,126,36), 38: (17,141,40), 39: (139,130,131), 40: (107,155,195), 44: (106,102,99), 46:(137,217,132), 47: (185,197,193), 50: (108,103,12), 52:(103,99,140)}


# These definitions all assume the folder structure for the data as delivered initially by Fugro, also see the readme
calibration_folder = configuration_dict["vision_data_path"] / "Video" / "Calibration"
laz_folder = configuration_dict["vision_data_path"] / "LAZ"
lidar_folders = [folder for folder in laz_folder.iterdir() if folder.is_dir()]
cam_position_folder = configuration_dict["vision_data_path"] / "ShapeFiles" / "Video"
lidar_tiles_folder = configuration_dict["vision_data_path"] / "ShapeFiles" / "LAZ"
video_data_folder = configuration_dict["vision_data_path"] / "Video"

def get_video_files(cam, calibration_folder_present=True):
    """
    This function assumes the default folder structure as defined in the start of this script

    It returns a list of tuples (runName, camNumber, [videoFiles]) for each run that is present in the data,
    in order to be able to find the positional data per run. The videofiles are also selected on cam number
    """

    result = []

    for run_folder in video_data_folder.iterdir():
        if run_folder.is_dir():
            if not (run_folder == calibration_folder or not calibration_folder_present) and run_folder.stem != "externalOrientation":
                run_name = run_folder.stem

                result.append((run_name, []))
                
                cam_folder = "cam" + str(cam)
                for video_file in (run_folder / cam_folder / cam_folder).iterdir():
                    result[-1][1].append(video_file)

    return result

def get_lidar_tiles():
    """
    This function returns a list of tuples (runName, [lidarTileFile]) that returns all lidar
    files necessary to do point projection.
    """

    result = []
    for run_folder in lidar_folders:
        run_name = run_folder.stem

        result.append((run_name, []))

        for lidar_tile_file in run_folder.iterdir():
            result[-1][1].append(lidar_tile_file)

    return result

def get_video_shapefile_bases():
    """
    This function is used to get a list of all the base files for all video shapefiles
    This only includes the files for which the cams are selected at the start of this script
    """

    result = set()

    for shapefile in cam_position_folder.iterdir():
        file_stem = shapefile.stem

        if int(file_stem[-1]) in configuration_dict["cams_to_use"]:
                result.add(file_stem)

    return list(result)

def get_video_viewpoint_data(cam, in_scope = True):
    """
    This function gets all data from the externalOrientation files. 
    Returns a list of tuples containing the same data as described by Luc, i.e.
    (time, easting, northing, elevation, roll, pitch, heading)
    with the frame name as key
    """

    result = []

    for run_folder in (video_data_folder / "externalOrientation").iterdir():

        result.append((run_folder.stem, {}))

        metadata_file = "cam" + str(cam) + (".mjpg_EO" + "_InScope" if in_scope else "") + ".csv"
        with open(run_folder / metadata_file, newline='') as csvfile:
            csvreader = csv.reader(csvfile, delimiter=',')
            for row in csvreader:
                newRow = []
                for entry in row[1:-1]:
                    newRow.append(float(entry))
                result[-1][1][row[0]] = tuple(newRow)

    return result

def get_camera_intrinsics(cam_number, file_name="3DOneCAM_20180706_201002303.xml"):
    """
    This function retrieves the camera intrinsics needed for point cloud point projection.
    """

    result = []
    intrinsics_file = video_data_folder / "Calibration" / file_name

    tree = et.parse(intrinsics_file)
    for cam in tree.getroot():
        if cam.attrib['id'] == str(cam_number):
            result.append(float(cam[0][0].attrib['x'])) # Camera center
            result.append(float(cam[0][0].attrib['y']))
            result.append(float(cam[0][1].attrib['x'])) # Focal length
            result.append(float(cam[0][1].attrib['y']))
            result.append(float(cam[0][2].attrib['k1'])) # Radial distortion
            result.append(float(cam[0][2].attrib['k2']))
            result.append(float(cam[0][3].attrib['p1'])) # Tangential distortion
            result.append(float(cam[0][3].attrib['p2']))
            
            break

    return tuple(result)

def get_rotation_matrix(roll, pitch, heading):
    roll = math.radians(roll)
    pitch = math.radians(pitch)
    heading = math.radians(heading)

    rz = np.array([[math.cos(heading), -1*math.sin(heading), 0], 
                   [math.sin(heading), math.cos(heading), 0],
                   [0, 0, 1]])
    
    ry = np.array([[math.cos(pitch), 0, math.sin(pitch)], 
                   [0, 1, 0], 
                   [-1 * math.sin(pitch), 0, math.cos(pitch)]])
    
    rx = np.array([[1, 0, 0],
                   [0, math.cos(roll), -1*math.sin(roll)], 
                   [0, math.sin(roll), math.cos(roll)]])

    return rx @ ry @ rz

def get_translation_matrix(tx, ty, tz):
    return np.float64([tx, ty, tz + configuration_dict["z_correction"]])

def get_camera_matrix(cx, cy, fx, fy):
    return np.array([[fx, 0, cx], [0, fy, cy], [0, 0, 1]])

def equation_plane(point1, point2, point3):
    x1, y1, z1 = point1
    x2, y2, z2 = point2
    x3, y3, z3 = point3
    a1 = x2 - x1
    b1 = y2 - y1
    c1 = z2 - z1
    a2 = x3 - x1
    b2 = y3 - y1
    c2 = z3 - z1
    a = b1 * c2 - b2 * c1
    b = a2 * c1 - a1 * c2
    c = a1 * b2 - b1 * a2
    d = (- a * x1 - b * y1 - c * z1)
    return a, b, c, d


def clip_points_planar(points, plane, flip=False):
    a, b, c, d = plane
    # print(points)
    clipped_points = []

    for point in points:
        # print(point)
        x, y, z = point[0], point[1], point[2]
        # Which side of plane
        if not flip:
            # Point is 'in front of' plane if true
            if a * x + b * y + c * z + d > 0:
                clipped_points.append(point)
        else:
            if a * x + b * y + c * z + d < 0:
                clipped_points.append(point)

    if len(clipped_points) == 0:
        return []
    clipped_points = np.vstack(clipped_points)
    # print(clipped_points)
    return clipped_points

def clip_points_radial(lidar_points, camera_origin, radius=configuration_dict["clipping_radius"]):

    clipped_points = []
    for point in lidar_points:
        dist = [(a - b)**2 for a, b in zip(point, camera_origin)]
        dist = math.sqrt(sum(dist))
        # print(dist)
        if dist <= radius:
            clipped_points.append(point)
    if len(clipped_points) == 0:
        return []
    clipped_points = np.vstack(clipped_points)
    return clipped_points

def get_lidar_files(viewpoint):
    """
    Function that returns the lidar file names that need to be included in a projection
    based on the camera viewpoint (in order to prevent a lot of memory copying etc)
    """

    tx = viewpoint[1]
    ty = viewpoint[2]

    laz_files = [p for p in laz_folder.rglob("*.laz")]
    laz_coordinates_dict = {}
    for laz in laz_files:
        coordinate_name = laz.stem
        x = int(coordinate_name[7:17])
        y = int(coordinate_name[-10:])
        laz_coordinates_dict[(x,y)] = laz

    x_down = tx - (tx % 25)
    y_down = ty - (ty % 25)

    result = []
    possible_tile_coords = [(x_down - 25, y_down - 25), (x_down, y_down - 25), (x_down + 25, y_down - 25), (x_down - 25, y_down), (x_down, y_down), (x_down + 25, y_down), (x_down - 25, y_down + 25), (x_down, y_down + 25), (x_down + 25, y_down + 25)]

    # print(possible_tile_coords)
    # print(tx, ty)
    
    for coord in possible_tile_coords:
        if coord in laz_coordinates_dict.keys():
            result.append(laz_coordinates_dict[coord])

    # print(result)
    return result

def get_video_file_path(run_name, cam, frame_number):
    return video_data_folder / run_name / ("cam" + str(cam)) / ("cam" + str(cam)) / (frame_number + ".jpg")

def project_points_on_image(run_name, cam, frame_number, frame_viewpoint, camera_intrinsics, radius):
    # print(f"\nProjecting run {run_name} from camera {cam} of {frame_number}\n")

    # print(frame_viewpoint)

    pitch = frame_viewpoint[-2]
    roll = frame_viewpoint[-3]
    heading = frame_viewpoint[-1]

    tx = frame_viewpoint[1]
    ty = frame_viewpoint[2]
    tz = frame_viewpoint[3]

    cx = camera_intrinsics[0]
    cy = camera_intrinsics[1]

    fx = camera_intrinsics[2]
    fy = camera_intrinsics[3]

    k1 = camera_intrinsics[4]
    k2 = camera_intrinsics[5]

    p1 = camera_intrinsics[6]
    p2 = camera_intrinsics[7]

    lidar_files = get_lidar_files(frame_viewpoint)
    # print(lidar_files)
    if len(lidar_files) == 0:
        return ([], [])
    las = laspy.read(lidar_files[0])
    lidar_points = np.vstack((las.x, las.y, las.z)).transpose()


    for i in range(len(lidar_files) - 1):
        las = laspy.read(lidar_files[i+1])

        current_points = np.vstack((las.x, las.y, las.z)).transpose()
        lidar_points = np.concatenate((lidar_points, current_points))

    # print(lidar_points)

    rvec = get_rotation_matrix(90 - pitch, -roll, heading)
    tvec = rvec.dot(get_translation_matrix(-tx, -ty, -tz))

    cam_matrix = get_camera_matrix(cx, cy, fx, fy)
    distortion = np.float64([k1, k2, p1, p2])

    camera_origin = np.float64([tx, ty, tz - configuration_dict["z_correction"]])
    camera_point_x = np.float64([1, 0, 0]) @ rvec + camera_origin  # Point on camera plane in x direction
    camera_point_y = np.float64([0, 1, 0]) @ rvec + camera_origin  # Point on camera plane in y direction
    camera_plane = equation_plane(camera_origin, np.ravel(camera_point_x), np.ravel(camera_point_y))
    lidar_points = clip_points_planar(lidar_points, camera_plane)
    if configuration_dict["clip_viewing_distance"]:
        lidar_points = clip_points_radial(lidar_points, camera_origin, radius)
    # print(lidar_points)

    if len(lidar_points) == 0:
        return ([],[])

    projected_points, _ = cv2.projectPoints(lidar_points, rvec, tvec, cam_matrix, distortion)
    projected_points = np.array([points[0] for points in projected_points])

    return (lidar_points, projected_points)
    # print(projected_points)

    # for p in projected_points:
    #     p = p.astype(np.int)
    #     point = (p[0][0], p[0][1])

    #     cv2.circle(image, point, 1, (0,0,0))
    
    # image = cv2.resize(image, (720,720))
    # cv2.imshow("Projected points on " + frame_number, image)
    # cv2.waitKey()

def get_detectron_predictor():
    cfg = get_cfg()

    cfg.merge_from_file(model_zoo.get_config_file("COCO-PanopticSegmentation/panoptic_fpn_R_101_3x.yaml"))
    cfg.MODEL.WEIGHTS = model_zoo.get_checkpoint_url("COCO-PanopticSegmentation/panoptic_fpn_R_101_3x.yaml")

    cfg.MODEL.DEVICE = configuration_dict["detectron_device"]

    predictor = DefaultPredictor(cfg)

    return predictor


# In the format needed for the "detections" of LDLS
def create_label_mask(video_frame):
    model_predictor = get_detectron_predictor()
    video_still = cv2.imread(video_frame.as_posix())

    model_prediction = model_predictor(video_still)
    model_output = model_prediction["sem_seg"].argmax(dim=0)
    model_output = model_output.numpy()

    return model_output

def turn_mask_binary(masks, highest_label):
    result = []
    for column in masks:
        partial_result = []
        for label in column:
            zs = np.zeros(highest_label, dtype=int)
            zs[label] = 1
            partial_result.append(zs)
        result.append(partial_result)
    
    return np.array(result)

def main_ldls():
    print("Projecting points since 2021, ldls")

    if not configuration_dict["clip_viewing_distance"]:
        configuration_dict["clipping_radius"] = [50]

    for radius in configuration_dict["clipping_radius"]:
        folder_title = "full_track_ldls"
        folder_title += "_reduced_radius_" + str(radius) + "m"

        # Testing project points on image
        for cam in configuration_dict["cams_to_use"]:
            cam_intrinsics = get_camera_intrinsics(cam)
            video_list = get_video_files(cam)
            viewpoint_data = get_video_viewpoint_data(cam)

            times_list = []

            for k in configuration_dict["run_folders_to_use"]:
                start = min(max(0, configuration_dict["video_start"]), len(video_list[k][1]))
                end = max(0, min(configuration_dict["video_end"], len(video_list[k][1])))

                for i in range(start, end, configuration_dict["video_step_size"]):
                    print(f"Projecting + ldls run {video_list[k][0]}, cam {cam} of frame {video_list[k][1][i].stem}")

                    t0 = time.time()
                    video_file_path = get_video_file_path(video_list[k][0], cam, video_list[k][1][i].stem)

                    model_output = create_label_mask(video_file_path)

                    projected_points = project_points_on_image(video_list[k][0], cam, video_list[k][1][i].stem, viewpoint_data[k][1][video_list[k][1][i].stem], cam_intrinsics, radius)
                    if len(projected_points[0]) == 0:
                        continue
                    ldls_splits = int(len(projected_points[0]) / configuration_dict["max_ldls_points"]) + 1
                    ldls_split_size = int(len(projected_points[0]) / ldls_splits)
                    # print(projected_points[1])
                    masks = turn_mask_binary(model_output, 60)

                    detections = ldls.MaskRCNNDetections(masks, None)
                    file_name_end = "labelled_point_cloud" + str(i) + ".txt"
                    os.makedirs(configuration_dict["point_cloud_output_folder"] / video_list[k][0] / ("cam" + str(cam)) / folder_title, exist_ok=True)
                    output_file = open(configuration_dict["point_cloud_output_folder"] / video_list[k][0] / ("cam" + str(cam)) /folder_title / file_name_end, "w")
                    output_file.close()

                    output_file = open(configuration_dict["point_cloud_output_folder"] / video_list[k][0] / ("cam" + str(cam)) / folder_title / file_name_end, "a")

                    for j in range(ldls_splits):
                        try:
                            lidarseg = ldls.LidarSegmentation(projected_points[1][(j * ldls_split_size) : ((j + 1) * ldls_split_size + 1)])
                            results = lidarseg.run(projected_points[0][(j * ldls_split_size) : ((j + 1) * ldls_split_size + 1)], detections, 50, save_all=False)
                        except:
                            continue

                        label_results = results.label_likelihoods
                        point_results = []
                        for point in label_results[1]:
                            labels = []
                            for h in range(len(point)):
                                if point[h]:
                                    labels.append(h)
                            point_results.append(labels)

                        for i in range(len(results.points)):
                            output_text = ""
                            output_text += str(results.points[i][0]) + " " + str(results.points[i][1]) + " " + str(results.points[i][2]) + " "

                            if point_results[i] == []:
                                output_text += "55"
                                color_r, color_g, color_b = (255, 255, 255)
                            else:
                                output_text += str(point_results[i][0] - 1)
                                try:
                                    color_r, color_g, color_b = COLOR_DICT[point_results[i][0] - 1]
                                except:
                                    color_r, color_g, color_b = (255, 255, 255)
                            output_text += " " + str(color_r) + " " + str(color_g) + " " + str(color_b)
                            output_text += '\n'
                            output_file.write(output_text)

                    if len(projected_points[0]) != 0:
                        time_spent = time.time() - t0
                        time_per_point = time_spent / len(projected_points[0])
                        times_list.append((time_spent, time_per_point))

                    # output_file.write(output_text)

            if configuration_dict["speed_measurements"]:
                unzipped = [[i for i, j in times_list], [j for i, j in times_list]]

                time_average = np.average(unzipped[0])
                time_std = np.std(unzipped[0])
                time_per_point_average = np.average(unzipped[1])
                per_point_std = np.std(unzipped[1])

                print(f"\nNumber of runs: {len(unzipped[0])}, Average time spent: {round(time_average, 2)} +- {round(time_std, 3)}, Average time spent per point: {round(time_per_point_average, 10)} +- {round(per_point_std, 10)}\n")

        

def main_projection():

    print("Projecting points since 2021, no ldls")

    if not configuration_dict["clip_viewing_distance"]:
        configuration_dict["clipping_radius"] = [50]

    for radius in configuration_dict["clipping_radius"]:
        folder_title = "full_track_projection"
        folder_title += "_reduced_radius_" + str(radius) + "m"

        # Testing project points on image
        for cam in configuration_dict["cams_to_use"]:
            cam_intrinsics = get_camera_intrinsics(cam)
            video_list = get_video_files(cam)
            viewpoint_data = get_video_viewpoint_data(cam)

            times_list = []

            for k in configuration_dict["run_folders_to_use"]:
                start = min(max(0, configuration_dict["video_start"]), len(video_list[k][1]))
                end = max(0, min(configuration_dict["video_end"], len(video_list[k][1])))
                
                for i in range(start, end, configuration_dict["video_step_size"]):
                    print(f"Projecting run {video_list[k][0]}, cam {cam} of frame {video_list[k][1][i].stem}")
                    
                    t0 = time.time()
                    video_file_path = get_video_file_path(video_list[k][0], cam, video_list[k][1][i].stem)

                    model_output = create_label_mask(video_file_path)

                    height = len(model_output)
                    width = len(model_output[0])

                    file_name_end = "labelled_point_cloud" + str(i) + ".txt"
                    os.makedirs(configuration_dict["point_cloud_output_folder"] / video_list[k][0] / ("cam" + str(cam)) / folder_title, exist_ok=True)
                    output_file = open(configuration_dict["point_cloud_output_folder"] / video_list[k][0] / ("cam" + str(cam)) / folder_title / file_name_end, "w")
                    output_file.close()

                    output_file = open(configuration_dict["point_cloud_output_folder"] / video_list[k][0] / ("cam" + str(cam)) / folder_title / file_name_end, "a")

                    # print(video_list[k][0], CAMS_TO_USE[0], video_list[k][1][i].stem, viewpoint_data[k][1][video_list[k][1][i].stem], cam_intrinsics)

                    point3ds, point2ds = project_points_on_image(video_list[k][0], cam, video_list[k][1][i].stem, viewpoint_data[k][1][video_list[k][1][i].stem], cam_intrinsics, radius)

                    if len(point3ds) != 0:
                        time_spent = time.time() - t0
                        time_per_point = time_spent / len(point3ds)
                        times_list.append((time_spent, time_per_point))
                    
                    for j in range(len(point3ds)):
                        point3d = point3ds[j]
                        point2d = point2ds[j]

                        # print(point3d, point2d)

                        point2d = point2d.astype(np.int32)

                        if point2d[0] >= 0 and point2d[0] < width and point2d[1] >= 0 and point2d[1] < height:
                            point_label = model_output[point2d[1]][point2d[0]]
                            try:
                                color_r, color_g, color_b = COLOR_DICT[point_label]
                            except:
                                color_r, color_g, color_b = (255, 255, 255)
                            output_text = str(point3d[0]) + " " + str(point3d[1]) + " " + str(point3d[2]) + " " + str(point_label) + " " + str(color_r) + " " + str(color_g) + " " + str(color_b) + "\n"
                            
                            output_file.write(output_text)
            
            if configuration_dict["speed_measurements"]:
                unzipped = [[i for i, j in times_list], [j for i, j in times_list]]

                time_average = np.average(unzipped[0])
                time_std = np.std(unzipped[0])
                time_per_point_average = np.average(unzipped[1])
                per_point_std = np.std(unzipped[1])

                print(f"\nNumber of runs: {len(unzipped[0])}, Average time spent: {round(time_average, 2)} +- {round(time_std, 3)}, Average time spent per point: {round(time_per_point_average, 10)} +- {round(per_point_std, 10)}\n")

def print_class_labels():
    model_predictor = get_detectron_predictor()
    d = MetadataCatalog.get(model_predictor.cfg.DATASETS.TRAIN[0])
    label_names = d.stuff_classes
    label_ids = list(d.stuff_dataset_id_to_contiguous_id.values())

    print(label_ids)

    labels_present = set([0,1,3,8,11,12,13,17,19,21,22,30,31,33,36,37,38,39,40,44,45,46,47,50,52])

    for i in range(len(label_names)):
        if i in labels_present:
            print(label_ids[i] - 1, label_names[i])

def check_label_presences():
    COMPLETED_POINT_CLOUD_FOLDER = Path("C:/Users/Philippe/Documents/studie/master_thesis/output/ep11-201002303-20190430-080329/full_track_projection")

    labels = [0 for i in range(60)]
    n = 1

    file_list = [f for f in COMPLETED_POINT_CLOUD_FOLDER.glob("*.txt")]
    for f in file_list:
        try:
            file = open(f, 'r')
        except:
            continue

        for line in file:
            words = line.split()
            label = int(words[-1])
            labels[label] = 1
        
        print(n, end='\r')
        n += 1

    for i in range(len(labels)):
        if labels[i]:
            print(i)

def create_detectron_images():
    print("Projecting points since 2021, making images")

    for cam in configuration_dict["cams_to_use"]:

        video_list = get_video_files(cam)

        model_predictor = get_detectron_predictor()

        for k in configuration_dict["run_folders_to_use"]:
            start = min(max(0, configuration_dict["video_start"]), len(video_list[k][1]))
            end = max(0, min(configuration_dict["video_end"], len(video_list[k][1])))
            
            for i in range(start, end, configuration_dict["video_step_size"]):
                print(f"Creating detectron2 image for run {video_list[k][0]}, cam {cam} of frame {video_list[k][1][i].stem}")

                video_file_path = get_video_file_path(video_list[k][0], cam, video_list[k][1][i].stem)

                model_output = create_label_mask(video_file_path)

                video_still = cv2.imread(video_file_path.as_posix())
                model_prediction = model_predictor(video_still)
                model_output = model_prediction["sem_seg"].argmax(dim=0)
                model_output = model_output.numpy()

                v = Visualizer(video_still[:, :, ::-1], MetadataCatalog.get(model_predictor.cfg.DATASETS.TRAIN[0]), instance_mode=ColorMode.SEGMENTATION)

                out = v.draw_sem_seg(model_output)
                # print(out.get_image()[:, :, ::-1])

                # out = cv2.resize(out.get_image()[:, :, ::-1], (720,720))
                out = out.get_image()[:, :, ::-1]

                file_name_end = "image_" + str(i) + ".jpg"
                os.makedirs(configuration_dict["point_cloud_output_folder"] / video_list[k][0] / ("cam" + str(cam)) / "detectron2_labelling", exist_ok=True)
                output_destination = configuration_dict["point_cloud_output_folder"] / video_list[k][0] / ("cam" + str(cam)) / "detectron2_labelling" / file_name_end
                cv2.imwrite(output_destination.as_posix(), out)

                # cv2.imshow("Test", out)
                # cv2.waitKey()

def show_point_projection():

    print("Projecting points since 2021")

    if not configuration_dict["clip_viewing_distance"]:
        configuration_dict["clipping_radius"] = [50]

    for radius in configuration_dict["clipping_radius"]:
        folder_title = "projection_images"
        folder_title += "_radius_" + str(radius) + "m"

        # Testing project points on image
        for cam in configuration_dict["cams_to_use"]:
            cam_intrinsics = get_camera_intrinsics(cam)
            video_list = get_video_files(cam)
            viewpoint_data = get_video_viewpoint_data(cam)

            for k in configuration_dict["run_folders_to_use"]:
                start = min(max(0, configuration_dict["video_start"]), len(video_list[k][1]))
                end = max(0, min(configuration_dict["video_end"], len(video_list[k][1])))
                
                for i in range(start, end, configuration_dict["video_step_size"]):
                    print(f"Projecting run {video_list[k][0]}, cam {cam} of frame {video_list[k][1][i].stem}")
                    
                    video_file_path = get_video_file_path(video_list[k][0], cam, video_list[k][1][i].stem)

                    video_still = cv2.imread(video_file_path.as_posix())

                    height = len(video_still)
                    width = len(video_still[0])

                    file_name_end = "point_cloud_projection_" + str(i) + ".jpg"
                    os.makedirs(configuration_dict["point_cloud_output_folder"] / video_list[k][0] / ("cam" + str(cam)) / folder_title, exist_ok=True)
                    output_destination = configuration_dict["point_cloud_output_folder"] / video_list[k][0] / ("cam" + str(cam)) / folder_title / file_name_end

                    point3ds, point2ds = project_points_on_image(video_list[k][0], cam, video_list[k][1][i].stem, viewpoint_data[k][1][video_list[k][1][i].stem], cam_intrinsics, radius)

                    for j in range(len(point3ds)):
                        point2d = point2ds[j]

                        # print(point3d, point2d)

                        point2d = point2d.astype(np.int32)

                        if point2d[0] >= 0 and point2d[0] < width and point2d[1] >= 0 and point2d[1] < height:
                            video_still = cv2.circle(video_still, point2d, 1, (0, 0, 0), -1)
                    
                    cv2.imwrite(output_destination.as_posix(), video_still)


if __name__ == "__main__":
    t0 = time.time()
    
    main_projection()

    main_ldls()

    # create_detectron_images()

    # check_label_presences()

    # print_class_labels()

    # show_point_projection()

    print("Total run time", time.time() - t0)