from pathlib import Path


GROUND_TRUTH_FILE = Path("C:/Users/Philippe/Documents/studie/master_thesis/output/ground_truth_platforms_cylindric.txt")

POINT_CLOUD_FOLDER = Path("C:/Users/Philippe/Documents/studie/master_thesis/output/ep11-201002303-20190430-080329/full_track_ldls")

LOWER_FILE_BOUND = 700

UPPER_FILE_BOUND = 1500

def main():
    print("Comparing ground truth at " + str(GROUND_TRUTH_FILE) + " with generated files in folder " + str(POINT_CLOUD_FOLDER))

    ground_truth_points = set()
    ground_truth_handler = open(GROUND_TRUTH_FILE)

    for line in ground_truth_handler:
        words = line.split()
        x = float(words[0])
        y = float(words[1])
        z = float(words[2])

        ground_truth_points.add((x, y, z))
    
    # print(ground_truth_points)
    
    generated_files = [f for f in POINT_CLOUD_FOLDER.rglob("*.txt")]
    counter = dict()
    n = 1

    total_points = 0
    for file in generated_files:
        print(n, end='\r')
        file_number = int(file.stem[48:])
        if file_number <= UPPER_FILE_BOUND and file_number >= LOWER_FILE_BOUND:
            file_handler = open(file)

            for line in file_handler:
                words = line.split()

                x = float(words[0])
                y = float(words[1])
                z = float(words[2])

                label = int(words[3])

                if (x, y, z) in ground_truth_points:
                    total_points += 1
                    if label in counter.keys():
                        counter[label] = counter[label] + 1
                    else:
                        counter[label] = 1
        n += 1

    print('\n')
    # print(counter)

    
    for label in counter:
        percentage = round(counter[label] / total_points * 100, 3)
        print(f"Label {label}: {counter[label]} -> {percentage}%")

if __name__ == "__main__":
    main()